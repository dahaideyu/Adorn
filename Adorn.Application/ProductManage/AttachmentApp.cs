using Adorn.Domain.Entity.ProductManage;
using Adorn.Domain.IRepository.ProductManage;
using Adorn.Repository.ProductManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Adorn.Application.ProductManage
{
    public class AttachmentApp
    {
        private IAttachmentRepository _service = new AttachmentRepository();

        public List<AttachmentEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<AttachmentEntity> GetList(Expression<Func<AttachmentEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public AttachmentEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(AttachmentEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(AttachmentEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
        public string  InsertForm(AttachmentEntity entity)
        {
            entity.Create();
            _service.Insert(entity);
            return entity.F_Id;
        }
    }
}
