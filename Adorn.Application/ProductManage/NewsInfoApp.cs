using Adorn.Domain.Entity.ProductManage;
using Adorn.Domain.Entity.SystemManage;
using Adorn.Domain.IRepository.ProductManage;
using Adorn.Domain.IRepository.SystemManage;
using Adorn.Repository.SystemManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Adorn.Application.ProductManage
{
    public class NewsInfoApp
    {
        private INewsInfoRepository _service = new NewsInfoRepository();

        public List<NewsInfoEntity> GetList()
        {
            return _service.IQueryable().OrderBy(t => t.F_CreatorTime).ToList();
        }
        public List<NewsInfoEntity> GetList(Expression<Func<NewsInfoEntity, bool>> expression)
        {
            return _service.IQueryable(expression).OrderByDescending(t => t.F_SortCode).ToList();
        }
        public NewsInfoEntity GetForm(string keyValue)
        {
            return _service.FindEntity(keyValue);
        }
        public void RemoveForm(NewsInfoEntity entity)
        {
            entity.Remove();
            _service.Update(entity);
        }
        public void DeleteForm(string keyValue)
        {
          _service.Delete(t => t.F_Id == keyValue);
        }
        public void SubmitForm(NewsInfoEntity entity, string keyValue)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                _service.Update(entity);
            }
            else
            {
                entity.Create();
                _service.Insert(entity);
            }
        }
    }
}
