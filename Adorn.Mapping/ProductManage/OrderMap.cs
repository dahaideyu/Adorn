using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class OrderMap : EntityTypeConfiguration<OrderEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderEntity>().ToTable("Sys_Order").HasKey(_ => _.F_Id);
        }
    }
}