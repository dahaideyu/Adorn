using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class ProductMap : EntityTypeConfiguration<ProductEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductEntity>().ToTable("Sys_Product").HasKey(_ => _.F_Id);
        }
    }
}