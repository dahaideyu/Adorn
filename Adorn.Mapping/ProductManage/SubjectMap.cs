using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class SubjectMap : EntityTypeConfiguration<SubjectEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubjectEntity>().ToTable("Sys_Subject").HasKey(_ => _.F_Id);
        }
    }
}