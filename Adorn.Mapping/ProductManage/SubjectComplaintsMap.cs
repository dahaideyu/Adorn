using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class SubjectComplaintsMap : EntityTypeConfiguration<SubjectComplaintsEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SubjectComplaintsEntity>().ToTable("Sys_SubjectComplaints").HasKey(_ => _.F_Id);
        }
    }
}