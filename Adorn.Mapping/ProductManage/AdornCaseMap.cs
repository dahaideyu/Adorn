using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class AdornCaseMap : EntityTypeConfiguration<AdornCaseEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdornCaseEntity>().ToTable("Sys_AdornCase").HasKey(_ => _.F_Id);
        }
    }
}