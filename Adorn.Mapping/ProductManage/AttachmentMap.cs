using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class AttachmentMap : EntityTypeConfiguration<AttachmentEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttachmentEntity>().ToTable("Sys_Attachment").HasKey(_ => _.F_Id);
        }
    }
}