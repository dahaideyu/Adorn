using Adorn.Domain.Entity.ProductManage;
using Microsoft.EntityFrameworkCore;

namespace Adorn.Mapping.ProductManage
{
    public class WorkerStatusLogMap : EntityTypeConfiguration<WorkerStatusLogEntity>
    {
        public override void Map(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkerStatusLogEntity>().ToTable("Sys_WorkerStatusLog").HasKey(_ => _.F_Id);
        }
    }
}