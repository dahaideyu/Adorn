using Adorn.Data;
using Adorn.Domain.Entity.ProductManage;

namespace Adorn.Domain.IRepository.ProductManage
{
    public interface IAttachmentRepository : IRepositoryBase<AttachmentEntity>
    {
    }
}
