using Adorn.Data;
using Adorn.Domain.Entity.ProductManage;

namespace Adorn.Domain.IRepository.ProductManage
{
    public interface IWorkerStatusRepository : IRepositoryBase<WorkerStatusEntity>
    {
    }
}
