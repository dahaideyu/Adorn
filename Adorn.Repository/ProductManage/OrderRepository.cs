using Adorn.Data;
using Adorn.Domain.Entity.ProductManage;
using Adorn.Domain.IRepository.ProductManage;
using Adorn.Repository.ProductManage;

namespace Adorn.Repository.ProductManage
{
    public class OrderRepository : RepositoryBase<OrderEntity>, IOrderRepository
    {
        
    }
}