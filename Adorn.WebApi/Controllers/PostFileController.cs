﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Adorn.Application.ProductManage;
using Adorn.Domain.Entity.ProductManage;
using Adorn.WebApi.Infrastructure;
using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using PMS.Code;

namespace Adorn.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostFileController : ControllerBase
    {
        private IMapper _mapper { get; set; }
        private IConfiguration _configuration { get; set; }
        private readonly ILogger<PostFileController> _logger;
        private IOptions<AppSettingsModel> _setting { get; set; }
        private AttachmentApp _attApp = new AttachmentApp();
        public PostFileController(IMapper mapper, IOptions<AppSettingsModel> settings, ILogger<PostFileController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _setting = settings;
        }
        [HttpPost("GetFiles")]
        public HttpResponseMessage Get(string fileName)
        {
            HttpResponseMessage result = null;
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            DirectoryInfo directoryInfo = new DirectoryInfo(basePath + @"Files/Pictures");
            FileInfo foundFileInfo = directoryInfo.GetFiles().Where(x => x.Name == fileName).FirstOrDefault();
            if (foundFileInfo != null)
            {
                FileStream fs = new FileStream(foundFileInfo.FullName, FileMode.Open);

                result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new StreamContent(fs);
                result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = foundFileInfo.Name;
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            return result;
        }


        [Produces("application/json")]
        [Consumes("application/json", "multipart/form-data")]//此处为新增
        [HttpPost("UploadFiles")]
        public async Task<ApiJsonResult> Post()
        {
            var imageSavePath = _setting.Value.ImageSavePath;
            try
            {
                //List<IFormFile> files
               var files = Request.Form.Files;
                _logger.LogError("sss");
                _logger.LogInformation("kaishi");
                _logger.LogWarning("警告信息");
                _logger.LogError("错误信息");
                long size = files.Sum(f => f.Length);
                DateTime dt = DateTime.Now;
                //var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var imgurl = "upload\\" + dt.ToString("yyyyMMdd") + "\\";
                var  basePath = imageSavePath + dt.ToString("yyyyMMdd")+"\\";
                _logger.LogError("imgurl:" + imgurl);
                _logger.LogError("basePath:" + basePath);
                string[] lines = { "basePath", basePath, "imgurl", imgurl };
                System.IO.File.WriteAllLines(imageSavePath+"\\test.txt", lines, Encoding.UTF8);
                if (!Directory.Exists(basePath))
                {
                    Directory.CreateDirectory(basePath);
                }

                List<string> ids = new List<string>();
                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                       

                        var filename = dt.ToString("yyyyMMddHHmmssffff");
                        var filePath = basePath+ filename +Path.GetExtension(formFile.FileName);
                        var urlpath = imgurl + filename + Path.GetExtension(formFile.FileName);
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                            AttachmentEntity aEntity = new AttachmentEntity();
                            aEntity.F_EnCode = Path.GetFileName(filePath);
                            aEntity.F_Type = Path.GetExtension(formFile.FileName);
                            ids.Add(urlpath);
                        }
                    }
                }
                var temp = new { count = ids.Count, size ,ids };
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success",Result= temp };

            }
            catch (Exception ex)
            {
                string[] lines = { "Message", ex.Message };
                System.IO.File.WriteAllLines(imageSavePath+"\\Message.txt", lines, Encoding.UTF8);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message };

            }


        }
        [Produces("application/json")]
        [Consumes("application/json", "multipart/form-data")]//此处为新增
        [HttpPost("UploadFilesById")]
        public async Task<ApiJsonResult> UploadFilesById([FromForm] IFormCollection formCollection)
        {
            var imageSavePath = _setting.Value.ImageSavePath;
            try
            {
                if (formCollection.ContainsKey("user"))
                {
                    var user = formCollection["user"];
                }
                //List<IFormFile> files
                var files = Request.Form.Files;
                _logger.LogError("sss");
                _logger.LogInformation("kaishi");
                _logger.LogWarning("警告信息");
                _logger.LogError("错误信息");
                long size = files.Sum(f => f.Length);
                DateTime dt = DateTime.Now;
                //var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var imgurl = "upload\\" + dt.ToString("yyyyMMdd") + "\\";
                var basePath = imageSavePath + dt.ToString("yyyyMMdd") + "\\";
                _logger.LogError("imgurl:" + imgurl);
                _logger.LogError("basePath:" + basePath);
                string[] lines = { "basePath", basePath, "imgurl", imgurl };
                System.IO.File.WriteAllLines(imageSavePath + "\\test.txt", lines, Encoding.UTF8);
                if (!Directory.Exists(basePath))
                {
                    Directory.CreateDirectory(basePath);
                }

                List<string> ids = new List<string>();
                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {


                        var filename = dt.ToString("yyyyMMddHHmmssffff");
                        var filePath = basePath + filename + Path.GetExtension(formFile.FileName);
                        var urlpath = imgurl + filename + Path.GetExtension(formFile.FileName);
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await formFile.CopyToAsync(stream);
                            AttachmentEntity aEntity = new AttachmentEntity();
                            aEntity.F_EnCode = Path.GetFileName(filePath);
                            aEntity.F_Type = Path.GetExtension(formFile.FileName);
                            ids.Add(urlpath);
                        }
                    }
                }
                var temp = new { count = ids.Count, size, ids };
                return new ApiJsonResult { IsSucceed = true, ErrorCode = "200", ErrorMessage = "success", Result = temp };

            }
            catch (Exception ex)
            {
                string[] lines = { "Message", ex.Message };
                System.IO.File.WriteAllLines(imageSavePath + "\\Message.txt", lines, Encoding.UTF8);
                return new ApiJsonResult { IsSucceed = false, ErrorCode = "500", ErrorMessage = ex.Message };

            }


        }
        /// <summary>
        /// 文件流的方式输出        /// </summary>
        /// <returns></returns>
        [HttpPost("DownFiles")]
        public IActionResult DownLoad(string file)
        {
            var addrUrl = file;
            var stream = System.IO.File.OpenRead(addrUrl);
            string fileExt = Path.GetExtension(file);
            //获取文件的ContentType
            var provider = new FileExtensionContentTypeProvider();
            var memi = provider.Mappings[fileExt];
            return File(stream, memi, Path.GetFileName(addrUrl));
        }
        #region old post
        //[HttpPost, Route("Upload")]
        //public async Task<string> Upload()
        //{
        //    try
        //    {
        //        var basePath = PlatformServices.Default.Application.ApplicationBasePath;
        //        DateTime dt = DateTime.Now;
        //        var folderName = dt.ToString("yyyyMMdd");
        //        var mapPath = basePath + "\\" + folderName;
        //        if (!Directory.Exists(mapPath))
        //        {
        //            // iRepositoryPath = parentPath + "\\" + areaName + "\\" + "\\IRepositories";
        //            Directory.CreateDirectory(mapPath);
        //        }
        //        //web api 获取项目根目录下指定的文件下

        //        var provider = new MultipartFormDataStreamProvider(mapPath);

        //        //文件已经上传  但是文件没有后缀名  需要给文件添加后缀名
        //        await Request.Body..Content.ReadAsMultipartAsync(provider);

        //        foreach (var file in provider.FileData)
        //        {
        //            //这里获取含有双引号'" '
        //            string filename = file.Headers.ContentDisposition.FileName.Trim('"');
        //            //获取对应文件后缀名
        //            string fileExt = filename.Substring(filename.LastIndexOf('.'));

        //            FileInfo fileinfo = new FileInfo(file.LocalFileName);
        //            //fileinfo.Name 上传后的文件路径 此处不含后缀名 
        //            //修改文件名 添加后缀名
        //            string newFilename = fileinfo.Name + fileExt;
        //            //最后保存文件路径
        //            string saveUrl = Path.Combine(root, newFilename);
        //            fileinfo.MoveTo(saveUrl);
        //        }
        //        return "success";
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        ////POST : api/Pictures
        //public async Task<IHttpActionResult> Post(IFormFile file)
        //{
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new Exception_DG("unsupported media type", 2005);
        //    }
        //    string root = IO_Helper_DG.RootPath_MVC;

        //    IO_Helper_DG.CreateDirectoryIfNotExist(root + "/temp");

        //    var provider = new MultipartFormDataStreamProvider(root + "/temp");

        //    // Read the form data.  
        //    await Request.Content.ReadAsMultipartAsync(provider);

        //    List<string> fileNameList = new List<string>();

        //    StringBuilder sb = new StringBuilder();

        //    long fileTotalSize = 0;

        //    int fileIndex = 1;

        //    // This illustrates how to get the file names.
        //    foreach (MultipartFileData file in provider.FileData)
        //    {
        //        //new folder
        //        string newRoot = root + @"Files/Pictures";

        //        IO_Helper_DG.CreateDirectoryIfNotExist(newRoot);

        //        if (File.Exists(file.LocalFileName))
        //        {
        //            //new fileName
        //            string fileName = file.Headers.ContentDisposition.FileName.Substring(1, file.Headers.ContentDisposition.FileName.Length - 2);

        //            string newFileName = Guid.NewGuid() + "." + fileName.Split('.')[1];

        //            string newFullFileName = newRoot + "/" + newFileName;

        //            fileNameList.Add($"Files/Pictures/{newFileName}");

        //            FileInfo fileInfo = new FileInfo(file.LocalFileName);

        //            fileTotalSize += fileInfo.Length;

        //            sb.Append($" #{fileIndex} Uploaded file: {newFileName} ({ fileInfo.Length} bytes)");

        //            fileIndex++;

        //            File.Move(file.LocalFileName, newFullFileName);

        //            Trace.WriteLine("1 file copied , filePath=" + newFullFileName);
        //        }
        //    }

        //    return Json(Return_Helper.Success_Msg_Data_DCount_HttpCode($"{fileNameList.Count} file(s) /{fileTotalSize} bytes uploaded successfully!     Details -> {sb.ToString()}", fileNameList, fileNameList.Count));
        //}
        [HttpPost]
        [Route("PostFileById")]
        public String PostFileById([FromForm] IFormCollection formCollection)
        {
            String result = "Fail";
            var imageSavePath = _setting.Value.ImageSavePath;
            string Id = "";
            if (formCollection.ContainsKey("Id"))
            {
                Id= formCollection["Id"];
            }
            DateTime dt = DateTime.Now;
            //Determine base path for the application.  
            var imgurl = "upload\\" + dt.ToString("yyyyMMdd") + "\\";
            var basePath = imageSavePath + dt.ToString("yyyyMMdd") + "\\";
            FormFileCollection fileCollection = (FormFileCollection)formCollection.Files;
            foreach (IFormFile file in fileCollection)
            {
                StreamReader reader = new StreamReader(file.OpenReadStream());
                String content = reader.ReadToEnd();
                String name = file.FileName;
                var filename = dt.ToString("yyyyMMddHHmmssffff");
                var filePath = basePath + filename + Path.GetExtension(file.FileName);
                var urlpath = imgurl + filename + Path.GetExtension(file.FileName);
         
                if (!Directory.Exists(basePath))
                {
                    // iRepositoryPath = parentPath + "\\" + areaName + "\\" + "\\IRepositories";
                    Directory.CreateDirectory(basePath);
                }
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                AttachmentEntity aEntity = new AttachmentEntity();
                aEntity.F_EnCode = Path.GetFileName(urlpath);
                aEntity.F_Type = Path.GetExtension(file.FileName);
                aEntity.F_OwnId = Id;
                aEntity.F_Path = urlpath;
                _attApp.SubmitForm(aEntity,null);
                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    // 复制文件  
                    file.CopyTo(fs);
                    // 清空缓冲区数据  
                    fs.Flush();
                }
                result = urlpath;
            }
            return result;
        }


        #endregion
    }

}
