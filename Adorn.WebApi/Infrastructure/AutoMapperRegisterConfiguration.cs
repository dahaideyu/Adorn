﻿using Adorn.Domain.Entity.SystemManage;
using Adorn.Domain.ViewModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Adorn.WebApi.Infrastructure
{
    public class AutoMapperRegisterConfiguration:Profile
    {
        public AutoMapperRegisterConfiguration()
        {
            CreateMap<APIUserVM, UserEntity>();
            CreateMap<UserEntity, APIUserVM>();
        }
    }

}
